import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def reading():
    data = pd.read_csv("pp_4l_all.csv")

    n = len(data)

    L1 = np.zeros((n, 4))
    L2 = np.zeros((n, 4))
    L3 = np.zeros((n, 4))
    L4 = np.zeros((n, 4))

    ID1 = data["pdg1"]
    L1[:,0] = data["E1"]
    L1[:,1] = data["p1x"]
    L1[:,2] = data["p1y"]
    L1[:,3] = data["p1z"]

    ID2 = data["pdg2"]
    L2[:,0] = data["E2"]
    L2[:,1] = data["p2x"]
    L2[:,2] = data["p2y"]
    L2[:,3] = data["p2z"]

    ID3 = data["pdg3"]
    L3[:,0] = data["E3"]
    L3[:,1] = data["p3x"]
    L3[:,2] = data["p3y"]
    L3[:,3] = data["p3z"]

    ID4 = data["pdg4"]
    L4[:,0] = data["E4"]
    L4[:,1] = data["p4x"]
    L4[:,2] = data["p4y"]
    L4[:,3] = data["p4z"]

    return ID1, ID2, ID3, ID4, L1, L2, L3, L4, n

def calculating_masses(L1, L2, L3, L4, n):

    M = np.zeros(n)

    for i in range(n):
        P0 = L1[i,0] + L2[i,0] + L3[i,0] + L4[i,0]
        P1 = L1[i,1] + L2[i,1] + L3[i,1] + L4[i,1]
        P2 = L1[i,2] + L2[i,2] + L3[i,2] + L4[i,2]
        P3 = L1[i,3] + L2[i,3] + L3[i,3] + L4[i,3]

        M[i] = P0**2 - P1**2 - P2**2 - P3**2

        if M[i] >= 0:
            M[i] = np.sqrt(M[i])
        else:
            M[i] = 0

    return M

def produce_pmf(Y, bins):

    m = len(Y)
    Y_pmf, edges = np.histogram(Y, bins=bins)

    Y_pmf = Y_pmf.astype(float)

    X_pmf = (edges[1:] + edges[:-1]) / 2.0

    return X_pmf, Y_pmf

def find_signal_boundaries(M):

    X_pmf, M_pmf = produce_pmf(M, (2*np.sqrt(len(M))).astype(int))

    max_index = np.argmax(M_pmf)
    print("Mass of the particle: ", "%.2f" % X_pmf[max_index], " - it is a Higgs")

    #I will search for 1/e^2 spectral width
    i = 0

    while True:
        if M_pmf[max_index+i] >= M_pmf[max_index]/(np.exp(1)**2):
            X_max = X_pmf[max_index+i]
        else:
            break
        i += 1

    i = 0

    while True:
        if M_pmf[max_index-i] >= M_pmf[max_index]/(np.exp(1)**2):
            X_min = X_pmf[max_index-i]
        else:
            break
        i += 1

    return X_min, X_max

def average_background(M):

    X_pmf, M_pmf = produce_pmf(M, (2*np.sqrt(len(M))).astype(int))
    is_not_in_range = np.where((X_pmf < X_min) | (X_pmf > X_max))[0]
    average_background = np.mean(M_pmf[is_not_in_range])

    return average_background

if __name__ == "__main__":
    ID1, ID2, ID3, ID4, L1, L2, L3, L4, n = reading()

    M_4l = calculating_masses(L1, L2, L3, L4, n)
    indices = np.where((M_4l >= 100) & (M_4l <= 150))[0]
    M = M_4l[indices] #M - masses in the interval [100, 150]
    L1 = L1[indices]
    L2 = L2[indices]
    L3 = L3[indices]
    L4 = L4[indices]

    X_min, X_max = find_signal_boundaries(M)

    is_in_range = M[(M >= X_min) & (M <= X_max)]

    #I haved made an assumption that the background is approximately the same across the entire [100,150] interval
    signal_rate = np.count_nonzero(is_in_range) - int(average_background(M))
    background_rate = len(M) - signal_rate

    print("Estimated integrated signal rate:", signal_rate)
    print("Estimated integrated background rate:", background_rate)
    print("Signal-to-Background ratio: ", "%.2f" % (signal_rate/background_rate))

    #Plotting the background and the signal in different colors to evaluate the results visually
    X_pmf, M_pmf = produce_pmf(M, (2*np.sqrt(len(M))).astype(int))
    bin_width = X_pmf[1]-X_pmf[0]

    for i in range(len(X_pmf)):

        if X_min <= X_pmf[i] <= X_max:
            color = 'red'

        else:
            color = 'blue'

        plt.bar(X_pmf[i], M_pmf[i], width=bin_width, color=color)


    plt.xlabel("$m_{4l}$ (GeV)")
    plt.ylabel("Counts")
    plt.xlim([100, 150])
    plt.title("$m_{4l}$ distribution (the signal is excluded from the background)")
    plt.savefig("p33.png")
    plt.savefig("p33.pdf")
    plt.close()
