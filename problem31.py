import pandas as pd #I have installed this module separately because I am used to working with pandas
import numpy as np
import matplotlib.pyplot as plt

def reading():
    data = pd.read_csv("pp_4l_all.csv")

    n = len(data)

    L1 = np.zeros((n, 4)) #L stands for lepton
    L2 = np.zeros((n, 4))
    L3 = np.zeros((n, 4))
    L4 = np.zeros((n, 4))

    ID_1 = data["pdg1"]
    L1[:,0] = data["E1"]
    L1[:,1] = data["p1x"]
    L1[:,2] = data["p1y"]
    L1[:,3] = data["p1z"]

    ID_2 = data["pdg2"]
    L2[:,0] = data["E2"]
    L2[:,1] = data["p2x"]
    L2[:,2] = data["p2y"]
    L2[:,3] = data["p2z"]

    ID_3 = data["pdg3"]
    L3[:,0] = data["E3"]
    L3[:,1] = data["p3x"]
    L3[:,2] = data["p3y"]
    L3[:,3] = data["p3z"]

    ID_4 = data["pdg4"]
    L4[:,0] = data["E4"]
    L4[:,1] = data["p4x"]
    L4[:,2] = data["p4y"]
    L4[:,3] = data["p4z"]

    return L1, L2, L3, L4, n

def calculating_masses(L1, L2, L3, L4, n):

    M = np.zeros(n)

    for i in range(n):
        P0 = L1[i,0] + L2[i,0] + L3[i,0] + L4[i,0]
        P1 = L1[i,1] + L2[i,1] + L3[i,1] + L4[i,1]
        P2 = L1[i,2] + L2[i,2] + L3[i,2] + L4[i,2]
        P3 = L1[i,3] + L2[i,3] + L3[i,3] + L4[i,3]

        M[i] = P0**2 - P1**2 - P2**2 - P3**2

        if M[i] >= 0:
            M[i] = np.sqrt(M[i])
        else:
            M[i] = 0

    return M

def plot_hist(dist, bins, title, name, x_label, y_label):
    plt.hist(dist, bins=bins, color="gray")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.xlim([20, 700])

    name_png = name + ".png"
    name_pdf = name + ".pdf"
    plt.savefig(name_png)
    plt.savefig(name_pdf)

    plt.close()

if __name__ == "__main__":
    L1, L2, L3, L4, n = reading()
    M_4l = calculating_masses(L1, L2, L3, L4, n)

    bins = 2*np.sqrt(len(M_4l))
    bins = bins.astype(int)
    plot_hist(M_4l, bins, "$m_{4l}$ distribution of problem 3.1", "p31", "$m_{4l}$ (GeV)", "Events")




