import numpy as np
import random
import matplotlib.pyplot as plt

def test(x1, x2, x3, x4):
    if (x1**4+4*x2**2+6*x3**2+8*x4**2 <= 40):
        return 1
    else:
        return 0

def MC_points(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m):

    x = np.zeros((m, 4))
    i = 0
    n = 0

    while i < m:

        x1 = random.uniform(x1_min, x1_max)
        x2 = random.uniform(x2_min, x2_max)
        x3 = random.uniform(x3_min, x3_max)
        x4 = random.uniform(x4_min, x4_max)
        n += 1

        if test(x1, x2, x3, x4) == 1:
            x[i, 0] = x1
            x[i, 1] = x2
            x[i, 2] = x3
            x[i, 3] = x4
            i += 1

    print(f"to reach 1E6 hits {n} points were generated")

    return x

def cross_section(x, m, a, b, c, d):

    distances = []

    for i in range(m):

        distance = np.sqrt(x[i,c-1]**2 + x[i,d-1]**2)
        distances.append(distance)

    indices = np.argsort(distances)[:int(0.1*m)]

    x_i = x[indices,a-1]
    x_j = x[indices,b-1]

    return x_i, x_j

def MC_points_for_cross_sections(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m2, T):

    x = np.zeros((m2, 4))
    i = 0

    while i < m2:

        if T[0] == 1:
            x1 = random.uniform(x1_min, x1_max)
        else:
            x1 = 0

        if T[1] == 1:
            x2 = random.uniform(x2_min, x2_max)
        else:
            x2 = 0

        if T[2] == 1:
            x3 = random.uniform(x3_min, x3_max)
        else:
            x3 = 0

        if T[3] == 1:
            x4 = random.uniform(x4_min, x4_max)
        else:
            x4 = 0

        if test(x1, x2, x3, x4) == 1:
            x[i, 0] = x1
            x[i, 1] = x2
            x[i, 2] = x3
            x[i, 3] = x4
            i += 1

    return x

if __name__ == "__main__":

    #Generating points for the i. part
    x1_min = -1*40**0.25
    x1_max = -1*x1_min
    x2_min = -1*np.sqrt(10)
    x2_max = -1*x2_min
    x3_min = -1*np.sqrt(20/3)
    x3_max = -1*x3_min
    x4_min = -1*np.sqrt(5)
    x4_max = -1*x4_min
    m = 1000000

    x = MC_points(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m)

    #Plotting the cross sections of the i. part
    fig, axis = plt.subplots(2, 3)

    x_i, x_j = cross_section(x, m, 1, 2, 3, 4)
    axis[0,0].scatter(x_i, x_j, s=0.001, c="green")
    axis[0,0].set_xlabel("$x_1$")
    axis[0,0].set_ylabel("$x_2$")

    x_i, x_j = cross_section(x, m, 1, 3, 2, 4)
    axis[0,1].scatter(x_i, x_j, s=0.001, c="green")
    axis[0,1].set_xlabel("$x_1$")
    axis[0,1].set_ylabel("$x_3$")

    x_i, x_j = cross_section(x, m, 1, 4, 2, 3)
    axis[0,2].scatter(x_i, x_j, s=0.001, c="green")
    axis[0,2].set_xlabel("$x_1$")
    axis[0,2].set_ylabel("$x_4$")

    x_i, x_j = cross_section(x, m, 2, 3, 1, 4)
    axis[1,0].scatter(x_i, x_j, s=0.001, c="green")
    axis[1,0].set_xlabel("$x_2$")
    axis[1,0].set_ylabel("$x_3$")

    x_i, x_j = cross_section(x, m, 2, 4, 1, 3)
    axis[1,1].scatter(x_i, x_j, s=0.001, c="green")
    axis[1,1].set_xlabel("$x_2$")
    axis[1,1].set_ylabel("$x_4$")

    x_i, x_j = cross_section(x, m, 3, 4, 1, 2)
    axis[1,2].scatter(x_i, x_j, s=0.001, c="green")
    axis[1,2].set_xlabel("$x_3$")
    axis[1,2].set_ylabel("$x_4$")

    fig.subplots_adjust(hspace=0.4, wspace=0.6)
    fig.suptitle("Cross sections in the $\pm$ 5% vicinity")

    fig.savefig("p23-i.png")
    fig.savefig("p23-i.pdf")
    plt.close()

    #generating points for the ii. part
    m2 = 100000

    x_12 = MC_points_for_cross_sections(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m2, [1,1,0,0])
    x_13 = MC_points_for_cross_sections(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m2, [1,0,1,0])
    x_14 = MC_points_for_cross_sections(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m2, [1,0,0,1])
    x_23 = MC_points_for_cross_sections(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m2, [0,1,1,0])
    x_24 = MC_points_for_cross_sections(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m2, [0,1,0,1])
    x_34 = MC_points_for_cross_sections(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, m2, [0,0,1,1])

    #Plotting the cross sections of the ii. part
    fig2, axis2 = plt.subplots(2, 3)

    axis2[0,0].scatter(x_12[:,0], x_12[:,1], s=0.001, c="green")
    axis2[0,0].set_xlabel("$x_1$")
    axis2[0,0].set_ylabel("$x_2$")

    axis2[0,1].scatter(x_13[:,0], x_13[:,2], s=0.001, c="green")
    axis2[0,1].set_xlabel("$x_1$")
    axis2[0,1].set_ylabel("$x_3$")

    axis2[0,2].scatter(x_14[:,0], x_14[:,3], s=0.001, c="green")
    axis2[0,2].set_xlabel("$x_1$")
    axis2[0,2].set_ylabel("$x_4$")

    axis2[1,0].scatter(x_23[:,1], x_23[:,2], s=0.001, c="green")
    axis2[1,0].set_xlabel("$x_2$")
    axis2[1,0].set_ylabel("$x_3$")

    axis2[1,1].scatter(x_24[:,1], x_24[:,3], s=0.001, c="green")
    axis2[1,1].set_xlabel("$x_2$")
    axis2[1,1].set_ylabel("$x_4$")

    axis2[1,2].scatter(x_34[:,2], x_34[:,3], s=0.001, c="green")
    axis2[1,2].set_xlabel("$x_3$")
    axis2[1,2].set_ylabel("$x_4$")

    fig2.subplots_adjust(hspace=0.4, wspace=0.6)
    fig2.suptitle("Cross sections of exactly (0,0) planes")

    fig2.savefig("p23-ii.png")
    fig2.savefig("p23-ii.pdf")
    plt.close()
