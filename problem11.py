import numpy as np
import random

def function(x):
    return x ** 4

def MC_integral(x_min, x_max, y_min, y_max, n):

    integral = 0

    for i in range(n):

        x = random.uniform(x_min, x_max)
        y = random.uniform(y_min, y_max)

        if (y <= function(x)):
            integral += 1

    V = (x_max-x_min)*(y_max-y_min)
    integral = integral * V / n

    return integral


if __name__ == "__main__":

    print("This code is quite slow because I am evaluating the error in a time-consuming way")

    Integral_values = []

    for i in range(40):
        Integral_values.append(MC_integral(-3, 3, 0, 3**4, 1000000))
        print(f"{i+1}/40 iterations completed", end='\r')

    err = np.std(Integral_values)

    print("Evaluation of the integral: ", "%.2f" % np.mean(Integral_values))
    print("Numeric error (standard deviation): ", "%.2f" % err)
