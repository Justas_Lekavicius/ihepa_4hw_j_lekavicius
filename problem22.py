import numpy as np
import random
import matplotlib.pyplot as plt


def test(x1, x2, x3, x4):
    if (x1**4+4*x2**2+6*x3**2+8*x4**2 <= 40):
        return 1
    else:
        return 0

def MC_volume(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, n):

    volume = 0
    V = (x1_max-x1_min)*(x2_max-x2_min)*(x3_max-x3_min)*(x4_max-x4_min)

    for i in range(n):

        x1 = random.uniform(x1_min, x1_max)
        x2 = random.uniform(x2_min, x2_max)
        x3 = random.uniform(x3_min, x3_max)
        x4 = random.uniform(x4_min, x4_max)

        volume += test(x1, x2, x3, x4)

    volume = volume * V / n

    return volume

def calculating_n(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max):

    Volume_values = np.zeros(50)
    n = 1000
    N_of_iterations = 200
    N = []
    dN = []
    n_lim = 0
    j = 1

    print("\nCalculating n for 1% error")

    while(True):
        Volume_values = np.zeros(N_of_iterations)

        for i in range(N_of_iterations):
            Volume_values[i] = MC_volume(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, n)

        err = np.std(Volume_values)
        rel_err = err / np.mean(Volume_values) * 100

        N.append(n)
        dN.append(rel_err)

        if rel_err <= 1:
            n_lim = n
            break

        print(f"{j} iterations completed (out of ~20)", end='\r')
        n += 1000
        j += 1

    plt.scatter(N, dN, c="g")
    plt.axhline(y=1, color='r', linestyle='--')
    plt.title("Relative error as a function of n")
    plt.xlabel("n")
    plt.ylabel("$\Delta n$ (%)")
    plt.savefig("p22.png")
    plt.savefig("p22.pdf")
    plt.close()

    print("n enough to achieve 1% relative error: ~", n_lim)


if __name__ == "__main__":

    x1_min = -1*40**0.25
    x1_max = -1*x1_min
    x2_min = -1*np.sqrt(10)
    x2_max = -1*x2_min
    x3_min = -1*np.sqrt(20/3)
    x3_max = -1*x3_min
    x4_min = -1*np.sqrt(5)
    x4_max = -1*x4_min
    n = 1000000

    print("This code is quite slow because I am evaluating the error in a time-consuming way")

    Volume_values = []

    for i in range(10):
        Volume_values.append(MC_volume(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max, n))
        print(f"{i+1}/10 iterations completed", end='\r')

    err = np.std(Volume_values)
    rel_err = err / np.mean(Volume_values) * 100

    print("Volume of the shape: ", "%.2f" % np.mean(Volume_values))
    print("Numeric error (standard deviation): ", "%.2f" % err)
    print("Relative error (%): ", "%.2f" % rel_err)

    calculating_n(x1_min, x1_max, x2_min, x2_max, x3_min, x3_max, x4_min, x4_max)
