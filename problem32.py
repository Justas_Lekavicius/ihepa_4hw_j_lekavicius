import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def reading():
    data = pd.read_csv("pp_4l_all.csv")

    n = len(data)

    L1 = np.zeros((n, 4))
    L2 = np.zeros((n, 4))
    L3 = np.zeros((n, 4))
    L4 = np.zeros((n, 4))

    ID1 = data["pdg1"]
    L1[:,0] = data["E1"]
    L1[:,1] = data["p1x"]
    L1[:,2] = data["p1y"]
    L1[:,3] = data["p1z"]

    ID2 = data["pdg2"]
    L2[:,0] = data["E2"]
    L2[:,1] = data["p2x"]
    L2[:,2] = data["p2y"]
    L2[:,3] = data["p2z"]

    ID3 = data["pdg3"]
    L3[:,0] = data["E3"]
    L3[:,1] = data["p3x"]
    L3[:,2] = data["p3y"]
    L3[:,3] = data["p3z"]

    ID4 = data["pdg4"]
    L4[:,0] = data["E4"]
    L4[:,1] = data["p4x"]
    L4[:,2] = data["p4y"]
    L4[:,3] = data["p4z"]

    return ID1, ID2, ID3, ID4, L1, L2, L3, L4, n

def calculating_masses(L1, L2, L3, L4, n):

    M = np.zeros(n)

    for i in range(n):
        P0 = L1[i,0] + L2[i,0] + L3[i,0] + L4[i,0]
        P1 = L1[i,1] + L2[i,1] + L3[i,1] + L4[i,1]
        P2 = L1[i,2] + L2[i,2] + L3[i,2] + L4[i,2]
        P3 = L1[i,3] + L2[i,3] + L3[i,3] + L4[i,3]

        M[i] = P0**2 - P1**2 - P2**2 - P3**2

        if M[i] >= 0:
            M[i] = np.sqrt(M[i])
        else:
            M[i] = 0

    return M

def Mass_2_particles(P1, P2):

    M = (P1[0]+P2[0])**2 - (P1[1]+P2[1])**2 - (P1[2]+P2[2])**2 - (P1[3]+P2[3])**2
    M = np.sqrt(M)

    return M

def Z_masses(L1, L2, L3, L4):

    M = np.zeros(4) #[M12, M14, M23, M24]
    Z1 = []
    Z2 = []

    for i in range(len(L1)):

        M[0] = Mass_2_particles(L1[i,:], L2[i,:])
        M[1] = Mass_2_particles(L1[i,:], L4[i,:])
        M[2] = Mass_2_particles(L2[i,:], L3[i,:])
        M[3] = Mass_2_particles(L2[i,:], L4[i,:])

        M_max = np.argmax(M)
        Z1.append(M[M_max])

        if M_max == 0:
            Z2.append(Mass_2_particles(L3[i,:], L4[i,:]))

        elif M_max == 1:
            Z2.append(Mass_2_particles(L2[i,:], L3[i,:]))

        elif M_max == 2:
            Z2.append(Mass_2_particles(L1[i,:], L4[i,:]))

        else:
            Z2.append(Mass_2_particles(L1[i,:], L3[i,:]))

    return Z1, Z2

def pT_and_rapidity(L1, L2, L3, L4):

    pT = np.zeros((len(L1), 4))
    rapidity = np.zeros((len(L1), 4))
    pT_temp = np.zeros(4)
    rapidity_temp = np.zeros(4)

    for i in range(len(L1)):

        pT_temp[0] = np.sqrt(L1[i, 1]**2 + L1[i, 2]**2)
        pT_temp[1] = np.sqrt(L2[i, 1]**2 + L2[i, 2]**2)
        pT_temp[2] = np.sqrt(L3[i, 1]**2 + L3[i, 2]**2)
        pT_temp[3] = np.sqrt(L4[i, 1]**2 + L4[i, 2]**2)

        #Found this formula on wikipedia. The formula with angle theta did not work for me (I was getting negative values inside ln function)
        rapidity_temp[0] = np.arctanh(L1[i,3]/np.sqrt(L1[i,1]**2+L1[i,2]**2+L1[i,3]**2))
        rapidity_temp[1] = np.arctanh(L2[i,3]/np.sqrt(L2[i,1]**2+L2[i,2]**2+L2[i,3]**2))
        rapidity_temp[2] = np.arctanh(L3[i,3]/np.sqrt(L3[i,1]**2+L3[i,2]**2+L3[i,3]**2))
        rapidity_temp[3] = np.arctanh(L4[i,3]/np.sqrt(L4[i,1]**2+L4[i,2]**2+L4[i,3]**2))

        indices = np.argsort(pT_temp)

        pT[i,:] = pT_temp[indices]
        rapidity[i,:] = rapidity_temp[indices]

    return pT, rapidity


if __name__ == "__main__":
    ID1, ID2, ID3, ID4, L1, L2, L3, L4, n = reading()

    M_4l = calculating_masses(L1, L2, L3, L4, n)
    indices = np.where((M_4l >= 100) & (M_4l <= 150))[0]
    M = M_4l[indices] #M - masses in the interval [100, 150]
    L1 = L1[indices]
    L2 = L2[indices]
    L3 = L3[indices]
    L4 = L4[indices]

    #Some explanations
    List1 = list(set(ID1))
    List2 = list(set(ID2))
    List3 = list(set(ID3))
    List4 = list(set(ID4))

    List = List1 + List2 + List3 + List4
    List = set(List)
    print("Unique ID values: ", List)
    print("Therefore, this set only contains e and mu leptons")
    print("List1:", List1)
    print("List2:", List2)
    print("List3:", List3)
    print("List4:", List4)
    print("Z is neutral, hence in this scenario I need to consider 1-2, 1-4, 2-3 and 3-4 particle pairs. A particle with the highest mass will be considered as a Z bozon.")

    #Z masses
    Z1, Z2 = Z_masses(L1, L2, L3, L4)

    #Plotting the results
    plt.hist(Z1, bins=int(2*np.sqrt(len(Z1))), color="green")
    plt.hist(Z2, bins=int(2*np.sqrt(len(Z2))), color="blue")
    plt.xlabel("$m_{Z}$ (GeV)")
    plt.ylabel("Events")
    plt.title("Mass distribution of Z bozons")

    plt.savefig("problem32-i.png")
    plt.savefig("problem32-i.pdf")
    plt.close()

    #pT and pseudorapidity calculations
    pT, rapidity = pT_and_rapidity(L1, L2, L3, L4)

    #Plotting pT
    fig, axis = plt.subplots(2, 2)

    axis[0,0].hist(pT[:,0], bins=int(2*np.sqrt(len(pT[:,0]))), color="red")
    axis[0,0].set_xlabel("$p_{T}$")
    axis[0,0].set_ylabel("$Events$")

    axis[0,1].hist(pT[:,1], bins=int(2*np.sqrt(len(pT[:,1]))), color="green")
    axis[0,1].set_xlabel("$p_{T}$")
    axis[0,1].set_ylabel("$Events$")

    axis[1,0].hist(pT[:,2], bins=int(2*np.sqrt(len(pT[:,2]))), color="blue")
    axis[1,0].set_xlabel("$p_{T}$")
    axis[1,0].set_ylabel("$Events$")

    axis[1,1].hist(pT[:,3], bins=int(2*np.sqrt(len(pT[:,3]))), color="grey")
    axis[1,1].set_xlabel("$p_{T}$")
    axis[1,1].set_ylabel("$Events$")

    fig.subplots_adjust(hspace=0.4, wspace=0.4)
    fig.suptitle("Distribution of transverse momentum")

    fig.savefig("p32-ii.png")
    fig.savefig("p32-ii.pdf")
    plt.close()


    #Ploting pseudorapidity
    fig2, axis2 = plt.subplots(2, 2)

    axis2[0,0].hist(rapidity[:,0], bins=int(2*np.sqrt(len(rapidity[:,0]))), color="red")
    axis2[0,0].set_xlabel("$\eta$")
    axis2[0,0].set_ylabel("$Events$")

    axis2[0,1].hist(rapidity[:,1], bins=int(2*np.sqrt(len(rapidity[:,1]))), color="green")
    axis2[0,1].set_xlabel("$\eta$")
    axis2[0,1].set_ylabel("$Events$")

    axis2[1,0].hist(rapidity[:,2], bins=int(2*np.sqrt(len(rapidity[:,2]))), color="blue")
    axis2[1,0].set_xlabel("$\eta$")
    axis2[1,0].set_ylabel("$Events$")

    axis2[1,1].hist(rapidity[:,3], bins=int(2*np.sqrt(len(rapidity[:,3]))), color="grey")
    axis2[1,1].set_xlabel("$\eta$")
    axis2[1,1].set_ylabel("$Events$")


    fig2.subplots_adjust(hspace=0.4, wspace=0.4)
    fig2.suptitle("Distribution of pseudorapidity")

    fig2.savefig("p32-iii.png")
    fig2.savefig("p32-iii.pdf")
    plt.close()
